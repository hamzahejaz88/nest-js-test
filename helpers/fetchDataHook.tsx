import { useState, useEffect } from "react";

const useLotteryData = (lotteryType: string) => {
	const [data, setData] = useState(null);
	const [loading, setLoading] = useState(true);
	const [error, setError] = useState(null);

	useEffect(() => {
		if (!lotteryType) return;

		const fetchLotteryData = async () => {
			setLoading(true);
			setError(null);

			try {
				const response = await fetch(
					`https://testing-luckito-backend.rnssol.com/api/luckito/lottery/get-lottery?lotteryType=${lotteryType}`,
				);
				if (!response.ok) {
					throw new Error("Response not ok");
				}
				const result = await response.json();
				setData(result);
			} catch (error: any) {
				setError(error?.message);
			} finally {
				setLoading(false);
			}
		};

		fetchLotteryData();
	}, [lotteryType]);

	return { lotteryData: data?.data, loading, error }; //
};

export default useLotteryData;
