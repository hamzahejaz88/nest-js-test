import { useEffect, useState } from "react";

interface Props {
	seconds: number | undefined;
}

export const TimeDisplay: React.FC<Props> = ({ seconds = 0 }) => {
	const [remainingSeconds, setRemainingSeconds] = useState(seconds);

	useEffect(() => {
		setRemainingSeconds(seconds);

		const intervalId = setInterval(() => {
			setRemainingSeconds((prevSeconds) => {
				if (prevSeconds <= 0) {
					clearInterval(intervalId);
					return 0;
				}
				return prevSeconds - 1;
			});
		}, 1000);

		return () => clearInterval(intervalId);
	}, [seconds]);

	const secondsToHMS = (seconds: number | undefined): string => {
		const absSeconds = Math.abs(seconds || 0);
		const hours = Math.floor(absSeconds / 3600);
		const minutes = Math.floor((absSeconds % 3600) / 60);
		const secs = absSeconds % 60;

		const formattedHours = String(hours).padStart(2, "0");
		const formattedMinutes = String(minutes).padStart(2, "0");
		const formattedSeconds = String(secs).padStart(2, "0");

		return `${formattedHours}:${formattedMinutes}:${formattedSeconds}`;
	};

	const formattedTime = secondsToHMS(remainingSeconds);

	return <div>{formattedTime}</div>;
};
