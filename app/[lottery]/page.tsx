/* eslint-disable react-hooks/rules-of-hooks */
"use client";
import React from "react";
import PlusIcon from "@/assets/Magnifier_Plus.svg";
import PlusIcon2 from "@/assets/Magnifier_Minus.svg";
import PlusIcon3 from "@/assets/Magnifier_PlusAqua.svg";
import { Card } from "@/components/Card/Card";
import useLotteryData from "@/helpers/fetchDataHook";
import { useSearchParams } from "next/navigation";

const LotteryPage = () => {
	const searchParams = useSearchParams();
	const {
		lotteryData: cosmicData,
		loading: cosmicLoading,
		error: cosmicError,
	} = useLotteryData("COSMIC");

	const {
		lotteryData: classicData,
		loading: classicLoading,
		error: classicError,
	} = useLotteryData("CLASSIC");
	const {
		lotteryData: atomicData,
		loading: atomicLoading,
		error: atomicError,
	} = useLotteryData("ATOMIC");
	return (
		<div className="flex min-h-screen flex-col items-center justify-between py-10">
			<Card
				data={cosmicData}
				loading={cosmicLoading}
				color="#EEE1F0"
				headingColor="#961A88"
				icon={PlusIcon}
				isAuthenticated={searchParams.get("authentication")}
			/>
			<Card
				data={classicData}
				loading={classicLoading}
				color="#E9EEF6"
				headingColor="#191978"
				icon={PlusIcon2}
				isAuthenticated={searchParams.get("authentication")}
			/>{" "}
			<Card
				data={atomicData}
				loading={atomicLoading}
				color="#EAF9F7"
				headingColor="#00AEB1"
				icon={PlusIcon3}
				isAuthenticated={searchParams.get("authentication")}
			/>
		</div>
	);
};
export default LotteryPage;
