"use client";

import { useEffect } from "react";
import { useRouter } from "next/navigation";

export default function Home() {
	const router = useRouter();

	useEffect(() => {
		const authenticateUser = () => {
			const username = window.prompt("Enter username:");
			const password = window.prompt("Enter password:");
			if (username === "admin" && password === "12345") {
				router.push(`/lottery?authentication=${true}`);
			} else {
				alert("Invalid credentials. Please refresh the page and try again.");
			}
		};

		authenticateUser();
	}, []);

	return <main></main>;
}
