describe('Home Component', () => {
  beforeEach(() => {
    cy.visit('/');
  });

  it('renders correctly with loading state', () => {
    cy.get('.loader').should('be.visible');
    cy.get('.card').should('not.exist'); // Ensure cards are not rendered while loading
  });

  it('renders cards with data', () => {
    cy.get('.card').should('have.length', 3);
  });

  it('displays card content when data is loaded', () => {
    cy.get('.loader').should('not.exist'); // Ensure loader is hidden
    cy.get('.card').each(($card) => {
      cy.wrap($card).find('.header').should('be.visible');
      cy.wrap($card).find('.lottery-number').should('be.visible');
      cy.wrap($card).find('.winning-pot-number').should('be.visible');
      cy.wrap($card).find('.next-draw').should('be.visible');
      cy.wrap($card).find('.card-footer').should('be.visible');
    });
  });

  it('expands and collapses card footer on click', () => {
    cy.get('.card-footer').should('not.be.visible');

    cy.get('.card').first().contains('Current Pool Status').click();
    cy.get('.card-footer').should('be.visible');

    cy.get('.card').first().contains('Close').click();
    cy.get('.card-footer').should('not.be.visible');
  });

  it('properly closes card footer when clicking "Close"', () => {
    cy.get('.card').first().contains('Current Pool Status').click();
    cy.get('.card-footer').should('be.visible');

    cy.get('.card').first().contains('Close').click();
    cy.get('.card-footer').should('not.be.visible');
  });

});
