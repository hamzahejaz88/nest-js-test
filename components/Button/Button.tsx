interface ButtonProps {
	onClick?: () => void;
	content: string;
}

export const Button: React.FC<ButtonProps> = ({ onClick, content }) => {
	return (
		<button
			type="button"
			className="rounded-md bg-white px-4 w-28 py-2.5 text-sm font-semibold hover:bg-slate-100 text-black"
			onClick={onClick}
		>
			{content}
		</button>
	);
};
