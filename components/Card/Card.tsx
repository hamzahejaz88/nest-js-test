import {
	CardFooter,
	Header,
	LotteryNumber,
	NextDraw,
	WinnigPotNumber,
} from "./ViewCardComponents";

interface LotteryData {
	lotteryName: string;
	lotteryPrice: number;
	roundNumber: number;
	previousWinningticket: number[];
	poolAmount: {
		poolId: string;
		lotteryId: string;
		coinId: string;
		coinSymbol: string;
		coinName: string;
		roundId: string;
		poolAmount: string;
		createdAt: string;
	}[];
	winningPot: number;
	currentPool: number;
	nextDraw: number;
	ticketCount: number;
	prevTicketCount: number;
	showPoolDetail: boolean;
}

interface CardProps {
	data: LotteryData | null;
	loading: boolean;
	color: string;
	headingColor: string;
	icon: string;
	isAuthenticated: string | null;
}

export const Card: React.FC<CardProps> = ({
	data,
	loading,
	color,
	headingColor,
	icon,
	isAuthenticated = false,
}) => {
	return (
		<div
			className="min-w-max w-[20%] flex flex-col gap-5 rounded-lg py-4"
			style={{ backgroundColor: color }}
		>
			{loading ? (
				<Loader />
			) : (
				<>
					<Header
						textColor={headingColor}
						title={data?.lotteryName}
						subTitle={data?.roundNumber}
						icon={icon}
					/>
					<LotteryNumber data={data} headingColor={headingColor} />
					<WinnigPotNumber data={data} />
					<NextDraw
						headingColor={headingColor}
						data={data}
						isAuthenticated={isAuthenticated}
					/>
					<CardFooter data={data} />
				</>
			)}
		</div>
	);
};
const Loader = () => {
	return (
		<div className="flex justify-center mt-[25%]">
			<div className="h-8 w-8 animate-spin rounded-full border-4 border-dashed border-gray-600" />
		</div>
	);
};
