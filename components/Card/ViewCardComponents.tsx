import { TimeDisplay } from "@/helpers/timerFunction";
import Image from "next/image";
import { useState } from "react";
import ClockIcon from "@/assets/clock.svg";
import { Button } from "../Button/Button";
import { useRouter } from "next/navigation";

interface HeaderProps {
	title?: string;
	subTitle?: number;
	icon?: string;
	textColor: string;
}
export const Header: React.FC<HeaderProps> = ({
	title,
	subTitle,
	icon,
	textColor,
}) => {
	return (
		<div className="flex items-center justify-between px-4">
			<div className="flex items-center gap-10" style={{ color: textColor }}>
				<span className="text-lg font-bold">{title}</span>
				<span className="text-sm font-medium">No.{subTitle}</span>
			</div>
			{icon && <Image src={icon} alt="Icon" />}
		</div>
	);
};
export const WinnigPotNumber = ({ data }: any) => {
	return (
		<div className="flex justify-between px-4">
			<span className="font-medium">Winning Pot</span>
			<span className="text-xs font-medium">
				<span className="font-bold text-lg">{data?.winningPot}</span>LUCKI
			</span>
		</div>
	);
};
export const LotteryNumber = ({ data, headingColor }: any) => {
	return (
		<div className="flex items-center justify-around">
			{data?.previousWinningticket.map((item: any, i: number) => (
				<div key={i}>
					<div
						className="rounded-full w-8 h-8 flex items-center justify-center text-white font-medium"
						style={{ backgroundColor: headingColor }}
					>
						{item}
					</div>
				</div>
			))}
		</div>
	);
};
export const NextDraw = ({ headingColor, data, isAuthenticated }: any) => {
	const router = useRouter();

	return (
		<div
			className="flex items-center justify-between px-4 py-2 text-white font-semibold"
			style={{ backgroundColor: headingColor }}
		>
			<div className="flex items-center gap-3">
				<span>Next Draw</span>
				<Image src={ClockIcon} alt="icon" />
				{data?.nextDraw && <TimeDisplay seconds={data?.nextDraw} />}
			</div>
			<Button
				content="Play"
				onClick={() => {
					if (isAuthenticated !== "true") {
						router.push("/");
					}
				}}
			/>
		</div>
	);
};
export const CardFooter = ({ data }: any) => {
	const [isExpanded, setIsExpanded] = useState(false);

	return (
		<>
			{!isExpanded && (
				<div
					className="flex justify-center cursor-pointer w-full font-semibold"
					onClick={() => setIsExpanded(!isExpanded)}
				>
					Current Pool Status
				</div>
			)}

			{isExpanded && (
				<div className="px-4 py-2">
					{data?.poolAmount !== undefined &&
						(data?.poolAmount?.length <= 0 || data?.poolAmount === null) && (
							<div className="flex w-full items-center justify-center font-semibold">
								No Data to show
							</div>
						)}
					{data?.poolAmount !== undefined &&
						data?.poolAmount?.length >= 0 &&
						data?.poolAmount?.map((item: any) => (
							<div className="flex justify-between" key={item?.coinId}>
								<span>{item?.coinSymbol}</span>
								<div className="flex gap-2 items-center font-medium">
									<span>{item?.poolAmount}</span>
									<span>{item?.coinName}</span>
								</div>
							</div>
						))}
					<div
						className="w-full flex justify-center mt-3 cursor-pointer font-semibold"
						onClick={() => setIsExpanded(!isExpanded)}
					>
						Close
					</div>
				</div>
			)}
		</>
	);
};
